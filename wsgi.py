#!/usr/bin/env python3
from TileStache import WSGITileServer
import os


base = os.path.dirname(__file__)
app = WSGITileServer(os.path.join(base, 'tilestache.cfg'))


if __name__ == '__main__':
    from werkzeug.serving import run_simple

    run_simple('127.0.0.1', 5000, app)
