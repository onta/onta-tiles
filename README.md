# Onta tiles

Servidor de tiles para el proyecto onta, usa tilestache y mapnik.

## Requisitos

* python 3
* mapnik
* mapnik bindings para python 3

## Instalación

* Crear un entorno virtual con pyhton 3 `virtualenv --system-site-packages .venv`
* `pip install -r requirements.txt`

## Ejecución

Correr dentro del entorno virtual:

    python wsgi.py
